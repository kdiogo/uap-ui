import React from 'react';
import { Story, Meta } from '@storybook/react/';
import Radio, { RadioProps, RadioSelections } from './Radio';

export default {
    title: 'Example/Radio',
    component: Radio,
} as Meta;

const radioSelections: RadioSelections<string>[] = [
    {
        text: 'Fuck',
        value: 'fuck',
        description: 'Choose this to fuck.',
    },
    {
        text: 'Marry',
        value: 'marry',
        description: 'Choose this to marry.',
    },
    {
        text: 'Kill',
        value: 'kill',
        description: 'Choose this to kill.',
    },
];

export const RadioStory: Story<RadioProps> = (args) => (
    <Radio
        {...args}
        containerClassName="text-gray-100"
        selections={radioSelections}
        name="Example Radio"
    />
);
