import React from 'react';
import cn from 'classnames';
import NavigationItem from "./NavigationItem";
import {
    faHome,
    faBlender,
    faMusic
} from '@fortawesome/free-solid-svg-icons'

type MobileNavProps = {
    className?: string
}

const MobileNav: React.FC<MobileNavProps> = ({
    className
}) => {
    return (
        <div className={cn('bg-gray-900 h-20 flex justify-center items-center space-x-8 w-screen overflow-auto z-10', className)}>
            <NavigationItem className="w-20" to="/" text="Home" icon={faHome} iconSize="lg" />
            <NavigationItem className="w-20" to="/blender" text="Blender" icon={faBlender} iconSize="lg" />
            <NavigationItem className="w-20" to="/blends" text="My Blends" icon={faMusic} iconSize="lg" />
        </div>
    );
};

export default MobileNav;
