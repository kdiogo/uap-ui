module.exports = {
	purge: [ './src/**/*.{js,jsx,ts,tsx}', './public/index.html' ],
	darkMode: false, // or 'media' or 'class'
	theme: {
		extend: {
			animation: {
				'pulse-intense': 'pulse-intense 1s cubic-bezier(0.4, 0, 0.6, 1) infinite'
			},
			boxShadow: {
				'outline-primary': '0 0 0 0.15rem rgb(235, 147, 123, .6)',
				'outline-red': '0 0 0 0.15rem rgba(220, 53, 69, .6)',
				'outline-secondary': '0 0 0 0.15rem rgba(106, 112, 124, .6)',
				'outline-green': '0 0 0 0.15rem rgba(72, 187, 120, 0.6)',
				'underline-primary': 'inset 0 -2px 0 #E36542',
				'underline-secondary': 'inset 0 -2px 0 #6A707C',
				'underline-red': 'inset 0 -2px 0 rgba(220, 53, 69, .6)',
				'underline-green': 'inset 0 -2px 0 rgba(72, 187, 120, 0.6)'
			},
			colors: {
				gray: {
					default: '#6A707C',
					'50': '#F8F8F8',
					'100': '#D6D8DC',
					'200': '#BABEC4',
					'300': '#9FA4AD',
					'400': '#838A95',
					'500': '#6A707C',
					'600': '#525760',
					'700': '#3B3E45',
					'800': '#232529',
					'900': '#0C0C0E'
				},
				/*'flame-pea'*/ primary: {
					50: '#FEF7F6',
					100: '#FCF0EC',
					200: '#F8D9D0',
					300: '#F4C1B3',
					400: '#EB937B',
					500: '#E36542',
					600: '#CC5B3B',
					700: '#883D28',
					800: '#662D1E',
					900: '#441E14'
				},
				spotify: '#1DB954',
				soundcloud: '#FE5000',
				youtube: '#c4302b'
			},
			keyframes: {
				'pulse-intense': {
					'0%, 100%': {
						opacity: 1
					},
					'50%': {
						opacity: 0.3
					}
				}
			},
			spacing: {
				'1/2': '50%',
				'1/3': '33.333333%',
				'2/3': '66.666667%',
				'1/4': '25%',
				'2/4': '50%',
				'3/4': '75%',
				'1/5': '20%',
				'2/5': '40%',
				'3/5': '60%',
				'4/5': '80%',
				'1/6': '16.666667%',
				'2/6': '33.333333%',
				'3/6': '50%',
				'4/6': '66.666667%',
				'5/6': '83.333333%',
				'1/12': '8.333333%',
				'2/12': '16.666667%',
				'3/12': '25%',
				'4/12': '33.333333%',
				'5/12': '41.666667%',
				'6/12': '50%',
				'7/12': '58.333333%',
				'8/12': '66.666667%',
				'9/12': '75%',
				'10/12': '83.333333%',
				'11/12': '91.666667%'
			}
		}
	},
	variants: {
		animation: [ 'responsive', 'hover' ]
	},
	plugins: []
};
