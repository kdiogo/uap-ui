import React from 'react';
import cn from 'classnames';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
    faUserCircle, faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'

export type ToolbarProps = {
    className: string;
};

const Toolbar: React.FC<ToolbarProps> = (props) => {
    return (
        <div className={cn('h-12 bg-gray-700 flex items-center p-3', props.className)}>
            <div className="ml-auto flex space-x-4 items-center">
                <button>
                    <FontAwesomeIcon icon={faUserCircle} size="1x" />
                </button>
                <button>
                    <FontAwesomeIcon icon={faSignOutAlt} size="1x" />
                </button>
            </div>
        </div>
    );
};

export default Toolbar;
