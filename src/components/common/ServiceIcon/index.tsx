import React from 'react';
import cn from 'classnames'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faSpotify,
    faSoundcloud,
    faYoutube,
} from '@fortawesome/free-brands-svg-icons';
import { SizeProp } from '@fortawesome/fontawesome-svg-core';

export type ServiceIconProps = {
    service: MusicService
    size: SizeProp;
    className?: string;
    styled? : boolean
};

const ServiceIcon: React.FC<ServiceIconProps> = ({
    styled = true,
    ...props
}) => {
    const iconMapping = {
        spotify: faSpotify,
        soundcloud: faSoundcloud,
        youtube: faYoutube,
    };

    const classMapping = {
        spotify: "text-spotify",
        soundcloud: "text-soundcloud",
        youtube: "text-youtube"
    }

    const styling = {
        [classMapping[props.service]]: styled
    }

    return (
        <React.Fragment>
            <FontAwesomeIcon
                className={cn(props.className, styling)}
                icon={iconMapping[props.service]}
                size={props.size}
            />
        </React.Fragment>
    );
};

export default ServiceIcon;
