import React from "react";
import cn from 'classnames'
import PlaylistCard from "./PlaylistCard";

export type PlaylistBrowserProps = {
    className?: string
}

const PlaylistBrowser: React.FC<PlaylistBrowserProps> = ({
    className
}) => {
    return (
        <div className={cn(className, 'grid grid-cols-2 xl:grid-cols-3 gap-3')}>
            <PlaylistCard service={"spotify"} title="Epic Anime OSTs" author="Kenny" />
            <PlaylistCard service={"spotify"} title="Best of Beatles" author="Nick" />
            <PlaylistCard service={"spotify"} title="Really Really Really Really Really Long Title" author="Kenny" />
            <PlaylistCard service={"spotify"} title="My Playlist" author="Kenny" />
        </div>
    )
}

export default PlaylistBrowser
