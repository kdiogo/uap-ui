import React from 'react'

export type PageWrapperProps = {
    title: string
}

const PageWrapper: React.FC<PageWrapperProps> = (props) => {
    return (
        <div>
            <h1 className="text-5xl mb-6 font-bold">{props.title}</h1>
            { props.children }
        </div>
    )
}

export default PageWrapper
