import { getQueryParams, sampleWithIndex, orderedMixArrays } from './index';
import { range } from 'lodash';

test('getQueryParams returns the correct object', () => {
    const queryObject = getQueryParams('param1=100&param2=200&param3=300');
    expect(queryObject).toEqual({
        param1: '100',
        param2: '200',
        param3: '300',
    });
});

test('sampleWithIndex returns the correct sample and index', () => {
    const sampleObject = sampleWithIndex(['kenny', 'nick', 'matt']);
    const validOutputs = new Set([
        { sample: 'kenny', sampleIndex: 0 },
        { sample: 'nick', sampleIndex: 1 },
        { sample: 'matt', sampleIndex: 2 },
    ]);
    expect(validOutputs).toContainEqual(sampleObject);
});

test('orderedMixArrays returns two arrays correctly ordered', () => {
    const arrays = [
        ['track1', 'track2', 'track3', 'track4', 'track5'],
        ['track6', 'track7', 'track8', 'track9', 'track10'],
        ['track11', 'track12', 'track13', 'track14', 'track15'],
        ['track16', 'track17', 'track18', 'track19', 'track20'],
    ];
    const orderedMixResults = orderedMixArrays(...arrays);
    const arrayPointers = range(arrays.length).map((_) => 0);
    for (const currentTrack of orderedMixResults) {
        let comparisonItem;
        arrays.forEach((arr, i) => {
            if (arr.includes(currentTrack)) {
                comparisonItem = arr[arrayPointers[i]];
                arrayPointers[i]++;
            }
        });
        expect(currentTrack).toBe(comparisonItem);
    }
});
