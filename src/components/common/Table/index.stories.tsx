import React from 'react';
import { Story, Meta } from '@storybook/react/';
import Table, { TableHeader, TableProps } from '.';

export default {
    title: 'Example/Table',
    component: Table,
} as Meta;

type StorybookTableRow = {
    id: string;
    title: string;
    artist: string;
    album: string;
};

const tableHeader: TableHeader<StorybookTableRow>[] = [
    {
        column: 'id',
        text: 'ID',
        sortable: true,
    },
    {
        column: 'title',
        text: 'Title',
        searchable: true,
        sortable: true,
    },
    {
        column: 'artist',
        text: 'Artist',
        searchable: true,
        sortable: true,
    },
    {
        column: 'album',
        text: 'Album',
        searchable: true,
        sortable: true,
    },
];

const data: StorybookTableRow[] = [
    {
        id: '1',
        title: 'Crawling After You',
        artist: 'Bass Drum of Death',
        album: 'Bass Drum of Death',
    },
    {
        id: '2',
        title: 'I Should Have Known Better',
        artist: 'The Beatles',
        album: "A Hard Day's Night",
    },
    {
        id: '3',
        title: 'Big Balls',
        artist: 'AC/DC',
        album: 'Dirty Deeds Done Dirt Cheap',
    },
];

export const TableStory: Story<TableProps<StorybookTableRow>> = (args) => (
    <Table<StorybookTableRow>
        {...args}
        className="text-gray-100"
        title="Example Title"
        headers={tableHeader}
        data={data}
    />
);
