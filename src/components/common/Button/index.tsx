import React from 'react';
import cn from 'classnames';

export type ButtonVariant = 'primary' | 'secondary' | 'danger' | 'success';
export type ButtonSize = 'sm' | 'md' | 'lg';
export type ButtonProps = {
    variant?: ButtonVariant;
    size?: ButtonSize;
    disabled?: boolean;
    className?: string;
    [x: string]: any;
};

const Button: React.FC<ButtonProps> = ({
    variant = 'primary',
    size = 'md',
    disabled = false,
    children,
    className,
    ...props
}) => {
    const baseClasses = [
        'font-bold',
        'rounded-lg',
        'transition-colors duration-75',
        'focus:outline-none',
    ].join(' ');

    // Variant classes
    const variantClasses: { [key: string]: any } = {
        primary: {
            base: ['bg-primary-500 focus:shadow-outline-primary'].join(' '),
            hover: ['hover:bg-primary-700'].join(' '),
        },
        secondary: {
            base: ['bg-gray-500 focus:shadow-outline-secondary'].join(' '),
            hover: ['hover:bg-gray-700'].join(' '),
        },
        danger: {
            base: ['bg-red-500 focus:shadow-outline-red'].join(' '),
            hover: ['hover:bg-red-700'].join(' '),
        },
        success: {
            base: ['bg-green-500 focus:shadow-outline-green'].join(' '),
            hover: ['hover:bg-green-700'].join(' '),
        },
    };

    // Size classes
    const smClasses = ['text-sm py-1 px-2'].join(' ');
    const mdClasses = ['py-2 px-4'].join(' ');
    const lgClasses = ['text-lg py-3 px-6'].join(' ');

    const buttonClasses = {
        [baseClasses]: true,
        [variantClasses[variant].base]: true,
        [variantClasses[variant].hover]: !disabled,
        [variantClasses[variant].focus]: !disabled,
        [smClasses]: size === 'sm',
        [mdClasses]: size === 'md',
        [lgClasses]: size === 'lg',
        'bg-opacity-50 focus:outline-none cursor-default': disabled,
    };

    return (
        <button
            className={cn(buttonClasses, className)}
            disabled={disabled}
            {...props}>
            {children}
        </button>
    );
};

export default Button;
