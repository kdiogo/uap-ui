import { useMediaQuery } from 'react-responsive'
const breakpoints = {
    'sm': '640px',
    'md': '768px',
    'lg': '1024px',
    'xl': '1280px',
    '2xl': '1536px',
}

export function useTailwindBreakpoints() {
    const is2Xl = useMediaQuery({ query: `(min-width: ${breakpoints["2xl"]})` })
    const isXl = useMediaQuery({ query: `(min-width: ${breakpoints["xl"]})` })
    const isLg = useMediaQuery({ query: `(min-width: ${breakpoints["lg"]})` })
    const isMd = useMediaQuery({ query: `(min-width: ${breakpoints["md"]})` })
    const isSm = useMediaQuery({ query: `(min-width: ${breakpoints["sm"]})` })

    return {
        is2Xl,
        isXl,
        isLg,
        isMd,
        isSm
    }
}
