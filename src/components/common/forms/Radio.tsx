import React from 'react';
import FormControl from './FormControl';
import { FormElementProps } from './form-control-config';

export interface RadioSelections<T> {
    value: T;
    text: string;
    description?: string;
}

export interface RadioProps extends FormElementProps {
    selections: RadioSelections<string>[];
}

const Radio: React.FC<RadioProps> = ({
    selections = [],
    // Inherited props
    disabled = false,
    label = '',
    name,
    margins = true,
    id,
    size = 'md',
    errorMessage,
    register,
    containerClassName,
    ...props
}) => {
    return (
        <FormControl
            label={label}
            margins={margins}
            errorMessage={errorMessage}
            className={containerClassName}>
            <div className="flex flex-col text-xl">
                {selections.map((selection) => (
                    <label
                        className="flex items-center mb-2"
                        key={selection.value}>
                        <input
                            type="radio"
                            name={name}
                            value={selection.value}
                            ref={register}
                        />
                        <div className="flex flex-col ml-3">
                            <span>{selection.text}</span>
                            {selection.description && (
                                <span className="text-sm">
                                    {selection.description}
                                </span>
                            )}
                        </div>
                    </label>
                ))}
            </div>
        </FormControl>
    );
};

export default Radio;
