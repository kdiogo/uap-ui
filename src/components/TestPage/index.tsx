import React, { useState } from 'react';
import Input from '../common/forms/Input';
import Button, { ButtonSize, ButtonVariant } from '../common/Button';
import Select from '../common/forms/Select';
import Radio, { RadioSelections } from '../common/forms/Radio';
import ServiceIcon from '../common/ServiceIcon';
import TableSkeleton from '../skeletons/TableSkeleton';
import Modal from '../common/Modal';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import Table, { TableHeader } from '../common/Table';

/**
 * Test page to see all shared/reusable components at a glance and
 * test functionality of them.
 */

const tableHeaders: TableHeader<any>[] = [
    { text: 'ID', column: 'id', sortable: true },
    { text: 'Track', column: 'track', searchable: true },
    { text: 'Artist', column: 'artist', sortable: true, searchable: true },
    {
        text: 'Album',
        column: 'album',
        render: (rowData) => (
            <span className="font-bold text-primary-600">{rowData.album}</span>
        ),
        searchable: true,
    },
];
const tableData = [
    { id: 1, track: 'All Star', artist: 'Smash Mouth', album: 'Apple' },
    { id: 2, track: 'Feel Good', artist: 'G-Eazy', album: 'Feel Good' },
    { id: 3, track: 'Sober', artist: 'Childish Gambino', album: 'Kauai' },
    {
        id: 4,
        track: '3005',
        artist: 'Childish Gambino',
        album: 'Because the Internet',
    },
    { id: 5, track: 'Black SpiderMan', artist: 'Logic', album: 'Everybody' },
    { id: 6, track: 'A.D.H.D', artist: 'Kendrick Lamar', album: 'Section.80' },
];

const formSchema = yup.object().shape({
    firstName: yup.string().required().label('First Name'),
    age: yup.number().positive().integer().required().label('Age'),
    gender: yup.string().required().label('Gender'),
    mixMethod: yup.string().required().label('Mix Method'),
});
export default function TestPage() {
    const { register, handleSubmit, errors } = useForm({
        resolver: yupResolver(formSchema),
    });
    const [modalOpen, setModalOpen] = useState(false);

    function onSubmit(data: any) {
        console.log(data);
    }

    const radioSelections: RadioSelections<
        'randomize' | 'beginning' | 'end'
    >[] = [
        {
            value: 'randomize',
            description:
                'Randomly mixes the songs from this playlist throughout the blend',
            text: 'Randomize',
        },
        {
            value: 'beginning',
            description:
                'Adds the entire playlist (in its current order) to the BEGINNING of the blend',
            text: 'Beginning',
        },
        {
            value: 'end',
            text: 'End',
            description:
                'Add the entire playlist (in its current order) to the END of the blend',
        },
    ];

    return (
        <div className="p-4 flex flex-col space-y-3">
            <form onSubmit={handleSubmit(onSubmit)}>
                <Input
                    name="firstName"
                    label="First Name"
                    register={register}
                    errorMessage={errors.firstName?.message}
                />
                <Input
                    block
                    type="number"
                    name="age"
                    label="Age"
                    register={register}
                    errorMessage={errors.age?.message}
                    min={0}
                />
                <Select
                    name="gender"
                    label="Gender"
                    register={register}
                    errorMessage={errors.gender?.message}>
                    <option value="M">Male</option>
                    <option value="F">Female</option>
                    <option value="NB">Non-binary</option>
                </Select>
                <Radio
                    selections={radioSelections}
                    name="mixMethod"
                    errorMessage={errors.mixMethod?.message}
                    label="Mix Method"
                    register={register}
                />
                <Button type="submit">Submit</Button>
            </form>

            <div>
                {(['sm', 'md', 'lg'] as ButtonSize[]).map((size) => (
                    <Button size={size} key={size}>
                        Test Button
                    </Button>
                ))}
                <Button disabled>Test Button</Button>
            </div>

            <div className="flex space-x-2">
                {([
                    'danger',
                    'primary',
                    'secondary',
                    'success',
                ] as ButtonVariant[]).map((variant) => (
                    <Button variant={variant} key={variant}>
                        Test Button
                    </Button>
                ))}
            </div>

            <TableSkeleton columns={4} rows={3} />

            <Table data={tableData} headers={tableHeaders} title="My Tracks" />

            <Button type="submit" onClick={() => setModalOpen(true)}>
                Open Modal
            </Button>
            {modalOpen && (
                <Modal title={'Fuck'}>
                    <Modal.Content>
                        <div>
                            <ServiceIcon
                                className="text-spotify"
                                service="spotify"
                                size="2x"
                            />
                            <ServiceIcon
                                className="text-soundcloud"
                                service="soundcloud"
                                size="2x"
                            />
                            <ServiceIcon
                                className="text-youtube"
                                service="youtube"
                                size="2x"
                            />
                        </div>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button
                            type="submit"
                            onClick={() => setModalOpen(false)}>
                            Ok
                        </Button>
                    </Modal.Actions>
                </Modal>
            )}
        </div>
    );
}
