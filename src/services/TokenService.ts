const storage = localStorage

// TODO: make interfaces for each token subsection
const tokenSerivce = {
    uap: {
        getAccessToken() {
            return storage.getItem("uapAccessToken")
        },
        setAccessToken(value: string) {
            storage.setItem("uapAccessToken", value)
        },
        getRefreshToken() {
            return storage.getItem("uapRefreshToken")
        },
        setRefreshToken(value: string) {
            storage.setItem("uapRefreshToken", value)
        },
        clearTokens() {
            storage.removeItem("uapRefreshToken")
            storage.removeItem("uapAccessToken")
        }
    },
    spotify: {
        getAccessToken() {
            return storage.getItem("spotifyAccessToken")
        },
        setAccessToken(value: string) {
            storage.setItem("spotifyAccessToken", value)
        },
        clearTokens() {
            storage.removeItem("spotifyRefreshToken")
            storage.removeItem("spotifyAccessToken")
        }
    }
}

export default tokenSerivce

export function clearAllTokens() {
    tokenSerivce.uap.clearTokens()
    tokenSerivce.spotify.clearTokens()
}
