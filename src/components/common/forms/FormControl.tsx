import React from 'react';
import cn from 'classnames';

type FormControlProps = {
    className?: string;
    label: string;
    errorMessage?: string;
    margins: boolean;
    forId?: string;
};

const FormControl: React.FC<FormControlProps> = ({
    className,
    margins = true,
    label,
    children,
    errorMessage,
    forId,
    ...props
}) => {
    return (
        <div className={cn({ 'mb-3': margins }, className)} {...props}>
            {label && (
                <label className="block font-bold mb-1" htmlFor={forId}>
                    {label}
                </label>
            )}

            <div className="relative">{children}</div>

            {errorMessage && (
                <p className="text-red-500 text-xs italic">{errorMessage}</p>
            )}
        </div>
    );
};

export default FormControl;
