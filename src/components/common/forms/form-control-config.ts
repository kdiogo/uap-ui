export type FormElementProps = {
    disabled?: boolean;
    label?: string;
    name: string;
    margins?: boolean;
    id?: string;
    register?: any;
    errorMessage?: string;
    containerClassName?: string;
    className?: string;
    [x: string]: any;
};

type FormControlSize = 'sm' | 'md' | 'lg';

export type SizeableFormElementProps = {
    size?: FormControlSize;
};

interface GetFormElementClassesProps {
    size: FormControlSize;
    state?: 'error' | 'success' | null;
    disabled: boolean;
}
export function useFormElementClasses({
    size,
    state,
    disabled,
}: GetFormElementClassesProps) {
    const baseClasses = [
        'w-full appearance-none',
        'outline-none border-none border-b border-red-500',
        'block shadow transition-color duration-200',
        'rounded-t leading-tight',
        'bg-transparent focus:bg-gray-700',
    ].join(' ');

    const sizeClasses: { [key: string]: any } = {
        sm: 'px-2 py-2 text-sm',
        md: 'px-2 py-3',
        lg: 'px-2 py-4 text-lg',
    };

    return {
        [baseClasses]: true,
        [sizeClasses[size]]: true,
        'shadow-underline-red': state === 'error',
        'shadow-underline-green': state === 'success',
        'shadow-underline-secondary focus:shadow-underline-primary': !state,
        'bg-gray-500': disabled,
    };
}
