import React, {useState} from "react";
import cn from 'classnames'

export type TabsProps = {
    className?: string
    tabHeaders: React.ReactNodeArray
    onChange?: (value: number) => any
}

const Tabs: React.FC<TabsProps> = ({
    className,
    tabHeaders,
    onChange,
    ...props
}) => {
    const [tabNumber, setTabNumber] = useState(0)

    function handleTabClick(i: number) {
        setTabNumber(i)
        if (onChange) onChange(i)
    }

    return (
        <div className={cn("flex space-x-2", className)}>
            {tabHeaders.map((e, i) => (
                <button key={i} onClick={() => handleTabClick(i)}
                    className={cn(
                        {'text-gray-400': i !== tabNumber},
                        {'border-b-2 border-primary-500':i === tabNumber },
                        'p-2 focus:outline-none'
                    )}>
                    {e}
                </button>
            ))}
        </div>
    )
}

export default Tabs
