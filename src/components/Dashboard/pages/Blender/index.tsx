import React from 'react';
import PageWrapper from '../PageWrapper';
import FindSongs from "./FindSongs";
import BlendEditor from "./BlendEditor";

function Blender() {
    return (
        <PageWrapper title="Blender">
            <div className="grid grid-cols-1 lg:grid-cols-2 gap-10">
                <FindSongs />
                <BlendEditor />
            </div>
        </PageWrapper>
    );
}

export default Blender
