import React, { ReactElement } from 'react';

export type ModalProps = {
    title: string;
    children: ReactElement[];
};

interface ModalComposition {
    Content: React.FC;
    Actions: React.FC;
}

const Content = () => null;
const Actions = () => null;

const Modal: React.FC<ModalProps> & ModalComposition = (props) => {
    const content = props.children.find((el) => el.type === Content);
    const actions = props.children.find((el) => el.type === Actions);

    return (
        <div className="fixed z-10 inset-0 overflow-y-auto">
            <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                <div className="fixed inset-0 transition-opacity">
                    <div className="absolute inset-0 bg-black opacity-75" />
                </div>
                {/*-- This element is to trick the browser into centering the modal contents.*/}
                <span className="hidden sm:inline-block sm:align-middle sm:h-screen" />
                &#8203;
                <div className="inline-block align-bottom bg-gray-700 rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                    <div className="px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div className="sm:flex sm:items-start">
                            <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                <h3
                                    className="text-2xl leading-6 font-medium"
                                    id="modal-headline">
                                    {props.title}
                                </h3>
                                <div className="mt-4">
                                    {content ? content.props.children : null}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="bg-gray-800 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse mt-3">
                        {actions ? actions.props.children : null}
                    </div>
                </div>
            </div>
        </div>
    );
};

Modal.Content = Content;
Modal.Actions = Actions;

export default Modal;
