import React from 'react';
import cn from 'classnames';

export type CardProps = {
    padding: boolean;
};

const Card: React.FC<CardProps> = (props) => {
    return (
        <div
            className={cn('bg-gray-900 rounded shadow-lg max-w-xs w-full', {
                'py-6 px-8': props.padding,
            })}>
            {props.children}
        </div>
    );
};

export default Card;
