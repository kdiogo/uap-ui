import React from 'react';
import { Story, Meta } from '@storybook/react/';
import Card, { CardProps } from '.';

export default {
    title: 'Example/Card',
    component: Card,
} as Meta;

export const CardStory: Story<CardProps> = (args) => (
    <div className="text-gray-100">
        <Card {...args}>Card</Card>
    </div>
);
