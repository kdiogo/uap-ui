import React from 'react';
import { Story, Meta } from '@storybook/react/';
import Input, { InputProps } from './Input';

export default {
    title: 'Example/Input',
    component: Input,
} as Meta;

const InputStory: Story<InputProps> = (args) => (
    <Input containerClassName="text-gray-100" {...args} name="Example Input" />
);

export const Default = InputStory.bind({});
Default.args = {
    placeholder: 'placeholder',
    label: 'label',
};

export const Error = InputStory.bind({});
Error.args = {
    placeholder: 'Error',
    label: 'Error',
    errorMessage: 'This is an error.',
    size: 'sm',
};

export const Block = InputStory.bind({});
Block.args = {
    placeholder: 'Block',
    label: 'Block',
    block: true,
};

export const Disabled = InputStory.bind({});
Disabled.args = {
    placeholder: 'Disabled',
    label: 'Disabled',
    disabled: true,
    size: 'lg',
};
