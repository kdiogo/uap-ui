import React from 'react';
import {UapTrack} from "../../../services/uap/types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import ServiceIcon from "../ServiceIcon";
import {faMusic} from "@fortawesome/free-solid-svg-icons";

type CommonTrackProps = {
    sideContent?: React.ReactNode
    onClick?: (track: UapTrack) => void
}

interface TrackItemProps extends CommonTrackProps {
    row: UapTrack
}
const TrackItem: React.FC<TrackItemProps> = ({
    row,
    sideContent,
    onClick
}) => (
    <button className="flex items-center" onClick={() => {if (onClick) return onClick(row)}}>
        <div className="mr-3">
            { row.imageUrl ? (
                <img src={row.imageUrl} alt="track cover" />
            ) : (
                <FontAwesomeIcon icon={faMusic} />
            )}
        </div>

        <div className="flex flex-col text-left">
            <span>
                {row.title} <ServiceIcon service={row.service} size={"xs"} />
            </span>
            <span className="text-xs text-gray-300">
                {row.artist}
            </span>
        </div>

        {sideContent &&
        <div className="ml-auto">
            { sideContent }
        </div>}
    </button>
)

interface TrackListProps extends CommonTrackProps {
    data: UapTrack[]
}
const TrackList: React.FC<TrackListProps> = ({
    data,
    sideContent,
    onClick
}) => {
    return (
        <div className="flex flex-col space-y-3">
            {data.map(row => (
                <TrackItem key={row.id} row={row} sideContent={sideContent} onClick={onClick} />
            ))}
        </div>
    );
}

export default TrackList
