import React from 'react';
import cn from 'classnames';
import FormControl from './FormControl';
import {
    FormElementProps,
    useFormElementClasses,
    SizeableFormElementProps,
} from './form-control-config';

export interface InputProps extends FormElementProps, SizeableFormElementProps {
    type?: string;
    placeholder?: string;
    block?: boolean;
}

const Input: React.FC<InputProps> = ({
    type = 'text',
    className,
    containerClassName,
    placeholder = '',
    block = false,
    // Inherited props
    disabled = false,
    label = '',
    name,
    margins = true,
    id,
    size = 'md',
    errorMessage,
    register,
    ...props
}) => {
    const commonClasses = useFormElementClasses({
        size,
        disabled,
        state: errorMessage ? 'error' : null,
    });

    return (
        <FormControl
            label={label}
            margins={margins}
            forId={id}
            errorMessage={errorMessage}
            className={containerClassName}>
            <input
                className={cn(
                    commonClasses,
                    { 'bg-gray-700': block },
                    className
                )}
                type={type}
                id={id}
                name={name}
                disabled={disabled}
                placeholder={placeholder}
                ref={register}
                {...props}
            />
        </FormControl>
    );
};

export default Input;
