import React from 'react';
import cn from 'classnames';
import {NavLink} from 'react-router-dom'
import {IconDefinition} from '@fortawesome/fontawesome-common-types'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {SizeProp} from '@fortawesome/fontawesome-svg-core'

export type SidebarItemProps = {
    to: string
    text: string
    icon?: IconDefinition
    iconSize?: SizeProp
    className?: string
    highlight?: boolean
};

const NavigationItem: React.FC<SidebarItemProps> = ({
    to,
    text,
    icon,
    iconSize = "1x",
    highlight = true,
    className
}) => {
    return (
        <NavLink to={to} activeClassName={highlight ? "text-primary-400" : ""} exact className={cn(
            "flex flex-col lg:flex-row justify-center items-center lg:justify-start",
            "hover:text-gray-100 hover:text-primary-500 transition-colors duration-75",
            "lg:space-x-4 space-y-1",
            className
        )}>
            { icon && <FontAwesomeIcon size={iconSize} icon={icon} /> }
            <span className="overflow-hidden whitespace-no-wrap overflow-ellipsis">{text}</span>
        </NavLink>
    );
};

export default NavigationItem;
