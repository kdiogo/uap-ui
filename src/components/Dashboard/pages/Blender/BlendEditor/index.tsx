import React from "react";
import Input from "../../../../common/forms/Input";
import Table, {TableHeader} from "../../../../common/Table";
import ServiceIcon from "../../../../common/ServiceIcon";
import Button from "../../../../common/Button";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {useTailwindBreakpoints} from '../../../../hooks/useTailwindBreakpoints'
import {
    faTimes
} from '@fortawesome/free-solid-svg-icons'
import {UapTrack} from "../../../../../services/uap/types";
import TrackList from "../../../../common/TrackList";

const mockData: UapTrack[] = [
    {id: '1', title: 'Cool Song', artist: 'Cool Artist', album: 'Cool Album', service: 'spotify'},
    {id: '2', title: 'Sandstorm', artist: 'Darude', album: 'Dududududu', service: 'soundcloud'}
]
function BlendEditor() {
    const { isLg } = useTailwindBreakpoints()
    const tableHeaders: TableHeader<UapTrack>[] = [
        {text: 'Title', column: 'title'},
        {text: 'Artist', column: 'artist'},
        {text: 'Album', column: 'album'},
        {
            text: 'Service', column: 'service',
            render: rowData => (
                <ServiceIcon service={rowData.service} size={"1x"} />
            )
        },
        {
            text: '',
            column: 'actions',
            render: rowData => (
                <Button size={"sm"} variant={"danger"}>
                    <FontAwesomeIcon icon={faTimes} />
                </Button>
            )
        }
    ]

    return (
        <div className="flex flex-col space-y-2">
            <div className="flex justify-center flex-col items-center">
                <h2 className="text-3xl">Blend Editor</h2>
                <Input className="text-lg text-center text-xl" placeholder="Name this blend..." name="blendName" />
            </div>

            { isLg ? (
                <Table<UapTrack> headers={tableHeaders} data={mockData} />
            ) : (
                <TrackList
                    data={mockData}
                    sideContent={<FontAwesomeIcon icon={faTimes} />}
                />
            )}

        </div>
    )
}

export default BlendEditor
