import React from 'react';
import cn from 'classnames';
import {PRODUCT_NAME} from "../../../constants";
import NavigationItem from "./NavigationItem";
import {
    faHome,
    faBlender,
    faMusic,
    faPlusCircle
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

type NavProps = {
    className?: string;
};

const Nav: React.FC<NavProps> = ({
    className
}) => {
    return (
        <div className={cn('bg-gray-900 w-64 p-4 flex flex-col overflow-auto', className)}>
            <h1 className="text-2xl mb-8 text-primary-500">{PRODUCT_NAME}</h1>

            <div className="space-y-4 text-xl mb-16">
                <NavigationItem to="/" text="Home" icon={faHome} />
                <NavigationItem to="/blender" text="Blender" icon={faBlender} />
            </div>

            <h2 className="text-xl mb-2 space-x-4">
                <span>My Blends</span>
                <FontAwesomeIcon icon={faMusic} />
            </h2>

            <hr className="mb-2" />

            <div className="space-y-2 text-lg">
                <div className="flex text-sm">
                    <div className="ml-auto">
                        <NavigationItem to="/blender" text="New Blend" highlight={false} icon={faPlusCircle} />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Nav;
