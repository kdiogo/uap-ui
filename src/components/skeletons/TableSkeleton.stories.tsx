import React from 'react';
import { Story, Meta } from '@storybook/react';
import TableSkeleton, { TableSkeletonProps } from './TableSkeleton';

export default {
    title: 'Example/TableSkeleton',
    component: TableSkeleton,
} as Meta;

const TableSkeletonStory: Story<TableSkeletonProps> = (args) => (
    <TableSkeleton {...args} />
);

export const Small = TableSkeletonStory.bind({});
Small.args = {
    rows: 2,
    columns: 2,
};

export const Medium = TableSkeletonStory.bind({});
Medium.args = {
    rows: 5,
    columns: 5,
};

export const Large = TableSkeletonStory.bind({});
Large.args = {
    rows: 15,
    columns: 6,
};
