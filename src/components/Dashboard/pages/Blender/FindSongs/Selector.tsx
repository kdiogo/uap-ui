import React from "react";
import cn from 'classnames'

export type SelectorItem<T> = {
    content: React.ReactNode
    label?: string
    value: T
    activeClass?: string
}

export type SelectorProps<T> = {
    onChange: (index: T) => any
    value: T
    className?: string
    items: SelectorItem<T>[]
    inactiveClass?: string
    label?: string
    labelClass?: string
}

function Selector<T>({
    onChange,
    value,
    className,
    items,
    inactiveClass,
    label,
    labelClass
}: SelectorProps<T>) {
    return (
        <div className={cn("flex flex-col space-y-2", className)}>
            <h3 className={cn("text-center text-2xl", labelClass)}>{label}</h3>
            <div className={cn("flex space-x-6 items-center")}>
                {items.map((item, i) => (
                    <button className={cn("flex flex-col items-center")} onClick={() => onChange(item.value)} key={i}>
                        <div className={cn(item.value === value ? item.activeClass : inactiveClass)}>
                            { item.content }
                        </div>
                        <span className="text-sm mt-2">{item.label}</span>
                    </button>
                ))}
            </div>
        </div>
    )
}

export default Selector
