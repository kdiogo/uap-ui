import { range, sumBy } from 'lodash';

export function getQueryParams(paramStr: string) {
    const params: { [key: string]: string } = {};
    paramStr.split('&').forEach((paramStr) => {
        const pair = paramStr.split('=');
        params[pair[0]] = pair[1];
    });
    return params;
}

export function sampleWithIndex<T>(arr: T[]) {
    const sampleIndex = Math.floor(Math.random() * arr.length);
    const sample = arr[sampleIndex];
    return {
        sample,
        sampleIndex,
    };
}

/**
 * Returns new array composed by
 * spreading items from array2 randomly throughout array1.
 * This means the items from array1 may not but
 * have the same index but will be guaranteed to keep their order relative
 * to the other items from array1.
 */
export function orderedMixArrays<T>(...args: T[][]) {
    // TODO: Optimize performance if needed
    // Potential bottlenecks:
    // - Sorting each index list
    // - Splicing when sampling without replacement

    const mixedSize = sumBy(args, (arr) => arr.length);
    // Build an array with n empty slots to be populated
    const mixedArray: (T | null)[] = range(mixedSize).map((_) => null);
    // List of indices the final array has that will be sampled from
    const mixedArrayIndices = range(mixedSize);

    args.forEach((arr, i) => {
        let currentArrayIndices: number[] = [];
        range(arr.length).forEach((j) => {
            // Sample a single index without replacement
            const { sample, sampleIndex } = sampleWithIndex(mixedArrayIndices);
            mixedArrayIndices.splice(sampleIndex, 1);
            // Add it to list of indicies array i will use
            currentArrayIndices.push(sample);
        });
        // Sort the list of indices to keep order
        currentArrayIndices = currentArrayIndices.sort((a, b) => a - b);
        // Populate the final mixed array with values from array i using the built list of sampled indicies
        currentArrayIndices.forEach((destIndex, arrIndex) => {
            mixedArray[destIndex] = arr[arrIndex];
        });
    });

    // Array is casted to contain no nulls since the algorithm
    // guarantees the population of every null slot
    return mixedArray as T[];
}
