import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/';
import Button, { ButtonProps } from '.';

export default {
    title: 'Example/Button',
    component: Button,
} as Meta;

const ButtonStory: Story<ButtonProps> = (args) => (
    <Button className="text-gray-100" {...args}>
        Button
    </Button>
);

export const Primary = ButtonStory.bind({});

export const Secondary = ButtonStory.bind({});
Secondary.args = {
    variant: 'secondary',
};

export const Danger = ButtonStory.bind({});
Danger.args = {
    variant: 'danger',
};

export const Success = ButtonStory.bind({});
Success.args = {
    variant: 'success',
};
