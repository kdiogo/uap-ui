import React from "react";
import cn from 'classnames'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHeadphones, faPlusCircle} from "@fortawesome/free-solid-svg-icons";
import ServiceIcon from "../../../../common/ServiceIcon";

export type PlaylistCardProps = {
    imgSrc?: string
    title: string
    author?: string
    service: MusicService
    className?: string
}

const PlaylistCard: React.FC<PlaylistCardProps> = ({
   className,
    imgSrc,
    title,
    service,
    author
}) => {
    return (
        <div className={cn(className)}>
            {/*<img className="shadow-xl" src={defaultPlaylistImage} />*/}
            <button className={cn(
                " flex justify-center items-center py-6 relative w-full",
                "bg-gray-700 text-gray-500 rounded shadow-lg",
                "transform hover:scale-105 transition duration-250"
            )}>
                <FontAwesomeIcon icon={faHeadphones} size={"8x"} />
                <FontAwesomeIcon className="absolute bottom-2 right-2" icon={faPlusCircle} />
                <ServiceIcon className="absolute bottom-2 left-2" service={service} size={"1x"} />
            </button>
            <div className="p-3 text-center flex flex-col">
                <span className="text-xl truncate">{title}</span>
                <span>By {author}</span>
            </div>
        </div>
    )
}

export default PlaylistCard
