import React from "react";
import cn from 'classnames'
import Table, {TableHeader} from "../../../../common/Table";
import Button from "../../../../common/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import {useTailwindBreakpoints} from "../../../../hooks/useTailwindBreakpoints";
import TrackList from "../../../../common/TrackList";
import {UapTrack} from "../../../../../services/uap/types";
import Input from "../../../../common/forms/Input";
import ServiceIcon from "../../../../common/ServiceIcon";


export type PlaylistBrowserProps = {
    className?: string
}
const tableHeaders: TableHeader<UapTrack>[] = [
    {column: 'title', text: 'Title'},
    {column: 'artist', text: 'Artist'},
    {column: 'album', text: 'Album'},
    {
        text: 'Service', column: 'service',
        render: rowData => (
            <ServiceIcon service={rowData.service} size={"1x"} />
        )
    },
    {
        column: 'actions',
        text: '',
        render: rowData => (
            <Button size={"sm"} variant={"success"}>
                <FontAwesomeIcon icon={faPlus} />
            </Button>
        )
    }
]
const mockData: UapTrack[] = [
    {title: "Faith", album: "Hybrid Theory", artist: "Linkin Park", id: "1", service: "spotify"},
    {title: "By Myself", album: "Hybrid Theory", artist: "Linkin Park", id: "2", service: "soundcloud"}
]

const SongBrowser: React.FC<PlaylistBrowserProps> = ({
    className,
}) => {
    const {isLg} = useTailwindBreakpoints()
    return (
        <div className={cn(className)}>
            <Input name="search-song" placeholder="Search songs across all services..." />
            {isLg ? (
                <Table<UapTrack> headers={tableHeaders} data={mockData} />
            ) : (
                <TrackList
                    data={mockData}
                    sideContent={<FontAwesomeIcon icon={faPlus} />}
                />
            )}
        </div>
    )
}

export default SongBrowser
