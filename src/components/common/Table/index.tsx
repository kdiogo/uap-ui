import React, { useState } from 'react';
import cn from 'classnames';
import TableSkeleton from '../../skeletons/TableSkeleton';
import { orderBy, filter, capitalize } from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
    faSortUp,
    faSortDown,
    faSort,
} from '@fortawesome/free-solid-svg-icons';
import Input from '../forms/Input';

export type SortDirection = 'none' | 'asc' | 'desc';
export type TableHeader<T> = {
    column: string;
    text: string;
    sortable?: boolean;
    searchable?: ((...args: any[]) => string | null) | boolean;
    render?: (rowData: T) => React.ReactElement;
};

export type TableProps<T> = {
    data?: any[];
    headers?: TableHeader<T>[];
    loading?: boolean;
    title?: string;
    className?: string;
};

const sortIconMapping = {
    none: faSort,
    asc: faSortUp,
    desc: faSortDown,
};

function Table<T> ({
    data = [],
    headers = [],
    loading = false,
    title,
    className,
    ...props
}: TableProps<T>) {
    const [sortingColumn, setSortingColumn] = useState('');
    const [sortingDirection, setSortingDirection] = useState<SortDirection>(
        'none'
    );
    const [searchText, setSearchText] = useState('');

    if (loading) {
        return <TableSkeleton columns={headers?.length} rows={4} />;
    }

    // Create mapping of headers array to avoid having to search when retrieving
    // values by the column value
    const headersMapping: { [key: string]: TableHeader<T> } = {};
    const searchableColumns = new Set<string>();
    for (const header of headers) {
        headersMapping[header.column] = header;
        if (header.searchable) {
            searchableColumns.add(header.column);
        }
    }

    function computeFilteredData() {
        let filteredData: any[];

        // Apply sorting
        if (!sortingColumn || sortingDirection === 'none') {
            filteredData = data;
        } else {
            filteredData = orderBy(data, [sortingColumn], [sortingDirection]);
        }

        // Apply search filtering
        if (searchableColumns.size > 0 && searchText) {
            filteredData = filter(
                filteredData,
                (item: { [key: string]: any }) => {
                    for (const key in item) {
                        if (!searchableColumns.has(key)) continue;

                        // If searchable is a function then invoke to get the correct value
                        const value =
                            typeof headersMapping[key].searchable === 'function'
                                ? String(
                                      (headersMapping[key] as any).searchable(
                                          item[key]
                                      )
                                  )
                                : String(item[key]);
                        if (
                            value
                                .toLowerCase()
                                .includes(searchText.toLowerCase())
                        ) {
                            return true;
                        }
                    }
                    return false;
                }
            );
        }
        return filteredData;
    }

    // Define toggle loop for sorting (ex. toggle sort when it is none changes it to up)
    const sortLoop: { [key: string]: SortDirection } = {
        none: 'asc',
        asc: 'desc',
        desc: 'none',
    };
    function handleSortClick(column: string) {
        setSortingDirection(sortLoop[sortingDirection]);
        // Start from up when switching sort column
        if (column !== sortingColumn) {
            setSortingDirection('asc');
        }
        setSortingColumn(column);
    }

    function handleSearchChange(e: React.ChangeEvent<HTMLInputElement>) {
        setSearchText(e.target.value);
    }

    return (
        <div className={cn(className, 'flex flex-col space-y-1')}>
            <span className="text-2xl font-bold">{title}</span>
            {searchableColumns.size > 0 && (
                <Input
                    containerClassName="w-1/4"
                    block
                    name="tableSearch"
                    size="sm"
                    placeholder="Search"
                    margins={false}
                    value={searchText}
                    onChange={handleSearchChange}
                />
            )}
            <table className="table-fixed">
                <thead>
                    <tr>
                        {headers?.map((header, i) => (
                            <th
                                key={i}
                                className="text-gray-300 border-b text-left py-4">
                                <span className="mr-2">
                                    {capitalize(header.text)}
                                </span>
                                {header.sortable && (
                                    <button
                                        className="focus:outline-none"
                                        onClick={() =>
                                            handleSortClick(header.column)
                                        }>
                                        {sortingColumn !== header.column ? (
                                            <FontAwesomeIcon icon={faSort} />
                                        ) : (
                                            <FontAwesomeIcon
                                                icon={
                                                    sortIconMapping[
                                                        sortingDirection
                                                    ]
                                                }
                                            />
                                        )}
                                    </button>
                                )}
                            </th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {computeFilteredData().map((row, i) => (
                        <tr key={i}>
                            {headers?.map((header, j) => (
                                <td key={j} className="border-b px-4 py-2">
                                    {header.render
                                        ? header.render(row)
                                        : row[header.column]}
                                </td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default Table;
