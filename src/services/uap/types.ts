export type UapTrack = {
    id: string
    title: string
    artist: string
    album: string
    imageUrl?: string
    service: MusicService
}
