import React from 'react';
import cn from 'classnames';
import FormControl from './FormControl';
import {
    FormElementProps,
    useFormElementClasses,
    SizeableFormElementProps,
} from './form-control-config';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCaretDown } from '@fortawesome/free-solid-svg-icons';

export interface SelectProps extends FormElementProps, SizeableFormElementProps {}

const Select: React.FC<SelectProps> = ({
    // Inherited props
    className,
    containerClassName,
    disabled = false,
    label = '',
    name,
    margins = true,
    id,
    size = 'md',
    errorMessage,
    register,
    ...props
}) => {
    const commonClasses = useFormElementClasses({
        size,
        disabled,
        state: errorMessage ? 'error' : null,
    });

    return (
        <FormControl
            label={label}
            margins={margins}
            errorMessage={errorMessage}
            className={containerClassName}>
            <select
                className={cn(commonClasses, 'pr-8 bg-gray-700', className)}
                name={name}
                disabled={disabled}
                ref={register}
                {...props}>
                {props.children}
            </select>
            <div className="pointer-events-none absolute right-2 top-0 transform translate-y-1/2">
                <FontAwesomeIcon icon={faCaretDown} />
            </div>
        </FormControl>
    );
};

export default Select;
