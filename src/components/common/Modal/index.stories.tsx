import React from 'react';
import { Story, Meta } from '@storybook/react';
import Modal, { ModalProps } from '.';
import Button from '../Button';

export default {
    title: 'Example/Modal',
    component: Modal,
} as Meta;

export const ModalStory: Story<ModalProps> = (args) => (
    <div className="text-gray-100">
        <Modal {...args}>
            <Modal.Content>
                Spicy jalapeno bacon ipsum dolor amet bresaola short loin eu
                dolor, brisket pork belly voluptate boudin dolore labore. Ex
                laborum bresaola short ribs. Flank fatback short ribs in shankle
                tenderloin dolore in pork belly adipisicing picanha rump nisi
                mollit. Nostrud t-bone ut, sint strip steak aute shankle turkey.
            </Modal.Content>
            <Modal.Actions>
                <Button>Submit</Button>
            </Modal.Actions>
        </Modal>
    </div>
);
