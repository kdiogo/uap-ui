import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom"

// Components Imports
import Dashboard from "./Dashboard"
import TestPage from "./TestPage";

function App() {
  return (
      <Router>
          <div className="bg-gray-800 text-gray-100 h-screen flex">
              <Switch>
                  <Route exact path="/test">
                      <TestPage />
                  </Route>
                  <Route path="/">
                      <Dashboard />
                  </Route>
              </Switch>
          </div>
      </Router>
  );
}

export default App;
