import React from 'react';
import { range } from 'lodash';

export type TableSkeletonProps = {
    columns: number;
    rows: number;
};

const TableSkeleton: React.FC<TableSkeletonProps> = (props) => {
    return (
        <table className="table-fixed animate-pulse w-full">
            <thead>
                <tr>
                    {range(props.columns).map((i) => (
                        <th
                            key={i}
                            className="text-gray-300 border-b text-left px-8 py-4">
                            <div className="w-1/2 bg-gray-300 h-5 rounded" />
                        </th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {range(props.rows).map((i) => (
                    <tr key={i}>
                        {range(props.columns).map((j) => (
                            <td key={j} className="border-b px-4 py-4">
                                <div className="w-full bg-gray-300 h-5 rounded" />
                            </td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default TableSkeleton;
