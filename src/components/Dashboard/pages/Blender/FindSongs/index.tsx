import React, {useState} from "react";
import Selector, {SelectorItem} from "./Selector";
import PlaylistBrowser from "./PlaylistBrowser";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
    faCompactDisc,
    faMusic
} from '@fortawesome/free-solid-svg-icons'
import SongBrowser from "./SongBrowser";

type MusicServiceBrowseMode = "playlist" | "song"
const browseModeSelectorItems: SelectorItem<MusicServiceBrowseMode>[] = [
    {
       value: "playlist",
        label: "My Playlists",
       content: <FontAwesomeIcon icon={faCompactDisc} size={"4x"} />
    },
    {
        value: "song",
        label: "Songs",
        content: <FontAwesomeIcon icon={faMusic} size={"4x"} />
    }
]

function FindSongs() {
    const [browseMode, setBrowseMode] = useState<MusicServiceBrowseMode>("playlist")
    return (
        <div className="flex flex-col space-y-4">
            <div className="flex flex-col lg:flex-row justify-center items-center space-y-6 lg:space-x-16 lg:space-y-0">
                <Selector<MusicServiceBrowseMode>
                    items={browseModeSelectorItems} label="Browse" inactiveClass="text-gray-700" labelClass="text-3xl"
                    onChange={setBrowseMode} value={browseMode}
                />
            </div>

            <div className="flex flex-col space-y-2">
                { browseMode === "playlist" ? <PlaylistBrowser /> : <SongBrowser /> }
            </div>
        </div>
    )
}

export default FindSongs
