const path = require('path');
const postCssConfig = require("../postcss.config")

module.exports = {
    "stories": [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)"
    ],
    "addons": [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        //"@storybook/preset-create-react-app"
    ],
    webpackFinal: async (config, {configType}) => {
        // Remove the existing css rule
        config.module.rules = config.module.rules.filter(
            f => {
                if (!f.test) return false
                return f.test.toString() !== '/\\.css$/'
            }
        );

        // Add in css rule that includes postcss
        config.module.rules.push({
            test: /\.css$/,
            loaders: [
                'style-loader',
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: postCssConfig
                }
            ],
            include: path.resolve(__dirname, '../')
        })
        return config
    }
}
