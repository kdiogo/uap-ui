import React from 'react';
import {
    Switch,
    Route
} from 'react-router-dom'
import Nav from "./navigation/Nav";
import MobileNav from "./navigation/MobileNav";
import Toolbar from './Toolbar';
import LoadingBar from './LoadingBar';
import Blender from "./pages/Blender";
import {useTailwindBreakpoints} from "../hooks/useTailwindBreakpoints";

export default function Dashboard() {
    const { isLg } = useTailwindBreakpoints()

    return (
        <div className="flex flex-col flex-grow">
            <div className="flex flex-grow min-h-0">
                { isLg && <Nav className="flex-shrink-0" /> }

                <div className="flex flex-col flex-grow">
                    <Toolbar className="flex-shrink-0" />
                    <LoadingBar />

                    <div className="p-4 lg:p-8 overflow-auto flex-grow">
                        <Switch>
                            <Route path={'/blender'}>
                                <Blender />
                            </Route>
                            <Route path={'/'}>
                                Hello
                            </Route>
                        </Switch>
                    </div>

                    { !isLg && <MobileNav className="flex-shrink-0" /> }
                </div>
            </div>
            {/* TODO: Put MusicPlayer component here */}
        </div>
    );
}
