import React from 'react';
import { Story, Meta } from '@storybook/react/';
import Select, { SelectProps } from './Select';

export default {
    title: 'Example/Select',
    component: Select,
} as Meta;

const SelectStory: Story<SelectProps> = (args) => (
    <Select {...args} name="Example Select" containerClassName="text-gray-100">
        <option value="spotify">Spotify</option>
        <option value="soundcloud">SoundCloud</option>
        <option value="youtube">YouTube</option>
    </Select>
);

export const Default = SelectStory.bind({});
Default.args = {
    placeholder: 'placeholder',
    label: 'label',
};

export const Error = SelectStory.bind({});
Error.args = {
    placeholder: 'Error',
    label: 'Error',
    errorMessage: 'This is an error.',
    size: 'sm',
};

export const Disabled = SelectStory.bind({});
Disabled.args = {
    placeholder: 'Disabled',
    label: 'Disabled',
    disabled: true,
    size: 'lg',
};
